import React, { useContext } from 'react';
import { BookContext } from '../context/BookContext';

const Navbar = () => {
    const {books} = useContext(BookContext);

    return ( 
        <div>
        <h1> Book List</h1>
        <p>{books.length}</p>
        </div>
     );
}
 
export default Navbar;